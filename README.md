# Goal
This tool takes a list of remote organizations or repositories endpoints defined within a YAML file and sync them to a GitLab instance and ultimately can be run as a cron job. In the backgroud it uses [bachp's git-mirror tool](https://github.com/bachp/git-mirror).

# Features
- Sync all or a list of sources repos from a GitHub organization or user
- Sync from direct Git repository endpoints to any specified GitLab group
- Exclude some previsouly synchronized repository from synchronization (see [`skip`](https://github.com/bachp/git-mirror#description-format) field)

# Requirements
- Python 3
- [bachp's git-mirror tool](https://github.com/bachp/git-mirror).

# Setup
To be able to run properly the tool needs:
- The configuration file `config.cfg`
- The list of GitHub organization (e.g. `ghsrc: bachp/git-mirror`), GitHub repositories (e.g. `ghsrc: bachp`) or any Git repositories URL (e.g. `https://github.com/xwiki/xwiki-platform.git`) in YAML format: `sourceRepos.yaml`.
- a GitLab user with:
  - a valid API key
  - an SSH public key attached to its profile, to push the repositories without password prompting.
- Properly configured GitLab _destination_ group(s) as to allow the tool to create project within:
  - The description field should contain `type: mirror`
  - To be able to create projects within, the GitLab user above should be granted `maintainer` access or more.

# Configuration file
See `config.cfg.dist` and copy it to `config.cfg` then replace with your own values to start over.

# Repository definitions file
See `sourceRepos.yaml.dist` for working examples.

# Development
Consider adding the following lines to your `~/.ssh/config` when working with several SSH keys pair:

    Host gitlab.my.host
      IdentityFile ~/.ssh/my-key-id

# FAQ
- Why the tool doesn't create and configure required GitLab **groups** on itself ?

  Because on the first iterations of the tool I didn't want it to be able create various groups in a GitLab instance without any control on it. I wanted the user to be fully aware of what groups is going to be created. On the top of that, creating and configuring a GitLab group is no more than a one-time operation.

- How to run the tool as a cron job ?

  On a Nix compatible system, one way is to create a dedicated "mirror" user along with a dedicated SSH key pair (without passphrase obviously), providing the public key to the related GitLab user. Then simply define a cronjob within that user.

## Troubleshooting
- `namespace is not valid` error : check if the gitlab user is a member of the target group
