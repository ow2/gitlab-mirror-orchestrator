#!/usr/bin/python3
# coding: utf8

# Copyright (c) 2017-present OW2 http://www.ow2.org
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import config
import requests
import json


def red(text):
    return ('\x1b[1;31;40m' + text + '\x1b[0m')


gitlabHeaders = {'private-token': config.globalCfg['gitlabApiKey']}


def getMirrorProjects():
    mirrorPIds = {}

    gitlabProjects = requests.get(
        config.globalCfg['gitlabApiUrl'] + '/projects?simple=1&per_page=100', headers=gitlabHeaders)
    if gitlabProjects.status_code == requests.codes.ok:
        for p in gitlabProjects.json():
            if 'origin: https' in p['description']:
                mirrorPIds[p['web_url']] = p['id']
        while 'next' in gitlabProjects.links:
            # get next page
            gitlabProjects = requests.get(
                gitlabProjects.links['next']['url'], headers=gitlabHeaders)
            if gitlabProjects.status_code == requests.codes.ok:
                for p in gitlabProjects.json():
                    if 'origin: https' in p['description']:
                        mirrorPIds[p['web_url']] = p['id']
            else:
                exit()
    else:
        print('oh oh...something wrong happened')

    return mirrorPIds


def deleteProtectedBranches():
    pIds = getMirrorProjects()

    for pUrl, pId in pIds.items():

        # uncomment and indent the action code below to go project by project
        s = input('process {} ({}) ?'.format(pUrl, pId))
        if s in ['y', 'Y']:

            print('## processing {} ({})'.format(pUrl, pId))

            # general project properties. Uses payload above.

            # r = requests.put(config.globalCfg['gitlabApiUrl'] + '/projects/{}'.format(
            # pId), params=payload, headers=gitlabHeaders)

            # protected branches lookup
            r = requests.get(config.globalCfg['gitlabApiUrl'] + '/projects/{}/protected_branches'.format(pId), headers=gitlabHeaders)

            protectedBranches = r.json()
            print(protectedBranches)
            # will unprotect all protected branches
            if len(protectedBranches):
                pBranches = [item['name'] for item in r.json()]
                print('{} : will unprotect -> {}'.format(pUrl, pBranches))
                for pBranch in pBranches:
                    r = requests.delete(
                    config.globalCfg['gitlabApiUrl'] + '/projects/{}/protected_branches/{}'.format(pId, pBranch), headers=gitlabHeaders)


def modifyProjectSettings():
    pIds = getMirrorProjects()

    payload = {
        'merge_requests_access_level': 'disabled',
        'builds_access_level': 'disabled',
        'auto_devops_enabled': False
    }
    #
    for pUrl, pId in pIds.items():

        # uncomment and indent the action code below to go project by project
        # s = input('process {} ({}) ?'.format(pUrl, pId))
        # if s in ['y', 'Y']:

        print('## processing {} ({})'.format(pUrl, pId))

        # general project properties. Uses payload above.

        r = requests.put(config.globalCfg['gitlabApiUrl'] + '/projects/{}'.format(
            pId), params=payload, headers=gitlabHeaders)


def deleteProjectWhenSourceProjectRemoved():
    with open('../potentialOrphans.json', 'r') as removedSourceRepos:
        try:
            _data = json.load(removedSourceRepos)
        except json.decoder.JSONDecodeError as e:
            print('problem loading JSON: {}'.format(e))

        _repos = _data['GitLabOrphan']
        for name, apiUrl in _repos.items():
            s = input('delete GitLab Repo {} ({}) ?'.format(name, apiUrl))
            if s in ['y', 'Y']:
                print('deleting...')
                try:
                    r = requests.delete(apiUrl, headers=gitlabHeaders)
                    r.raise_for_status()
                except requests.exceptions.HTTPError as e:
                    print("There was a problem deleting project {}: {}".format(name, e))


def main():
    # uncomment the needed call
    # deleteProjectWhenSourceProjectRemoved()
    deleteProtectedBranches()


if __name__ == '__main__':
    main()
