import sys
# import argparse

from configparser import ConfigParser, ExtendedInterpolation

cfg = ConfigParser(interpolation=ExtendedInterpolation())
cfg.read(sys.path[0] + '/config.cfg')
globalCfg = cfg['global']

# parser = argparse.ArgumentParser(description="Git to GitLab mirroring tool")
#
# # group = parser.add_mutually_exclusive_group(required=True)
# parser.add_argument("-gs", "--show-groupsize", help="show the total size of the repos in github", action="store_true")
# args = parser.parse_args()
