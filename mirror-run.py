#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Convenience wrapper for running gitlab-mirror-orchestrator directly from source tree."""

# from gitlabmirator.mirrorIt import main
import gitlamirator.mirrorIt

if __name__ == '__main__':
    gitlamirator.mirrorIt.main()
