#!/usr/bin/python3

import unittest
from gitlamirator import mirrorIt


class MirrorItTest(unittest.TestCase):
    def test_build_ref_spec_args(self):
        refspecs = mirrorIt.build_ref_spec_args(
            '+refs/heads/*:refs/heads/*,+refs/tags/*:refs/tags/*')
        self.assertEqual(refspecs,
                         ['--refspec', '+refs/heads/*:refs/heads/*',
                             '--refspec', '+refs/tags/*:refs/tags/*']
                         )
        refspecs = mirrorIt.build_ref_spec_args('')
        self.assertFalse(refspecs)

    def test_get_issues_enabled_for(self):
        item = {'issues_enabled_for': ['repo1', 'repo2', 'repo3']}
        issues_enabled_for = mirrorIt.get_issues_enabled_for(item)
        self.assertIsInstance(issues_enabled_for, list)
        self.assertEqual(len(issues_enabled_for), 3)

        item = ['wrong_input']
        issues_enabled_for = mirrorIt.get_issues_enabled_for(item)
        self.assertEqual(issues_enabled_for, False)

        item = {'issues_enabled_for': 'test string'}
        issues_enabled_for = mirrorIt.get_issues_enabled_for(item)
        self.assertEqual(issues_enabled_for, None)

    def test_get_src_type(self):
        # Valid
        item = {'ghsrc': 'bachp/git-mirror'}
        src_type = mirrorIt.get_src_type(item)
        self.assertEqual(src_type, 'ghsrc')

        # Invalid multiple source type declared
        with self.assertRaises(ValueError) as cm:
            item = {'ghsrc': 'foo', 'directsrc': 'bar'}
            src_type = mirrorIt.get_src_type(item)
        self.assertEqual('Only one type of source is allowed (ghsrc for GitHub or directsrc for Direct). Found several for: {}'.format(item), str(cm.exception))

        # Invalid no source type declared
        with self.assertRaises(ValueError) as cm:
            item = {'foo': 'bar'}
            src_type = mirrorIt.get_src_type(item)
        self.assertEqual('No source type found for: {}'.format(item), str(cm.exception))
