#!/usr/bin/python3
# coding: utf8

# Copyright (c) 2017-present OW2 http://www.ow2.org
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from . import config
import requests
import pprint
import logging
import json
pp = pprint.PrettyPrinter()
logger = logging.getLogger(__name__)


def red(text):
    return ('\x1b[1;31;40m' + text + '\x1b[0m')


# authentication against gitlab
gitlabHeaders = {'private-token': config.globalCfg['gitlabApiKey']}

sessionGihubApi = requests.Session()
if 'gitHubApiToken' in config.globalCfg:
    sessionGihubApi.headers.update(
        {'Authorization': 'token {}'.format(config.globalCfg['gitHubApiToken'])})

# the cache is shared over all functions
cacheGroup = {}

# the orphan file
orphanFilePath = f'{config.base_dir}/potentialOrphans.json'

try:
    with open(orphanFilePath, 'w') as orphanFile:
        json.dump({'GitLabOrphan': {}}, orphanFile)
except OSError as e:
    logger.error(f'cannot create file: {e}')


class GitlabGroupError(Exception):
    def __init__(self, message):
        self.message = message


class GithubError(Exception):
    def __init__(self, message):
        self.message = message


def record_orphan(entry):
    """
    entry : a dict of orphans to record as :
    {'proactive/documentation': 'https://gitlab.ow2.org/api/v4/projects/383',
     'proactive/scheduling': 'https://gitlab.ow2.org/api/v4/projects/382'}

    The output is a JSON file
    the filename is hardcoded in orphanFilePath
    """
    if type(entry) is not dict:
        return False
    try:
        with open(orphanFilePath, 'r+') as orphanFile:
            _j = json.load(orphanFile)
            orphanFile.seek(0)
            _j['GitLabOrphan'].update(entry)
            json.dump(_j, orphanFile)
    except OSError as e:
        logger.error('cannot update file: {}'.format(e))
    except json.decoder.JSONDecodeError:
        logger.error("JSON decode error")


def cache_gitlab_group(namespace):
    """return True when the cache has been updated sucessfully"""

    def get_meta(namespace):
        try:
            gitlabGroupMeta = requests.get(
                config.globalCfg['gitlabApiUrl'] + '/groups/{}?simple=1&with_projects=0'.format(namespace), headers=gitlabHeaders)
            gitlabGroupMeta.raise_for_status()

            cacheGroup['meta'] = gitlabGroupMeta.json()
            return True
        except requests.exceptions.HTTPError as e:
            logger.error("There was a problem in getting group's metadata : {}".format(e))
            cacheGroup.clear()
            return False

    def get_projects(namespace):
        try:
            gitlabGroup = requests.get(
                config.globalCfg['gitlabApiUrl'] + '/groups/{}/projects?simple=1&per_page=100'.format(namespace), headers=gitlabHeaders)
            gitlabGroup.raise_for_status()
            cacheGroup['projects'] = []
            # items from first page
            cacheGroup['projects'].extend([p for p in gitlabGroup.json()])
            while 'next' in gitlabGroup.links:
                # get next page until there is one
                gitlabGroup = requests.get(
                    gitlabGroup.links['next']['url'], headers=gitlabHeaders)
                gitlabGroup.raise_for_status()
                cacheGroup['projects'].extend([p for p in gitlabGroup.json()])
            return True
        except requests.exceptions.HTTPError as e:
            logger.error("There was a problem in getting group's projects : {}".format(e))
            cacheGroup.clear()
            return False

    def do_update(namespace):
        logger.debug(f'updating group cache for {namespace}')
        cacheGroup.clear()
        # first we collect the group metadata only
        if not get_meta(namespace):
            return False
        # then collect group's projects names
        if not get_projects(namespace):
            return False
        return True

    if 'meta' in cacheGroup:
        # GitLab group's 'path' (in json response) is always lowercase. However, the user maintained YML source
        # definition can have dstgroup with uppercase chars.
        # In addition, gitlab isn't sensitive to the case of groups, ie:
        # (API) https://gitlab.ow2.org/api/v4/groups/PRELUDE is the same as https://gitlab.ow2.org/api/v4/groups/prelude
        # and :
        # (Web) https://gitlab.ow2.org/PRELUDE is redirected to https://gitlab.ow2.org/prelude
        # so we do all the compute in lowercase

        if cacheGroup['meta']['path'].lower() == namespace.lower():
            # cache is already populated
            return True
        else:
            # new project : update needed
            return do_update(namespace)
    else:
        # cache miss this group, updating
        return do_update(namespace)


def add_project_to_gitlab(namespace, repoName, repoUrl, issuesEnabledFor=False):
    """
    Add a mirror project to an *existing* group.

    The destination namespace should exists beforehand
    """
    logger.debug("invoke add project {}/{} ...".format(namespace, repoName))

    # processing only if the cache is populated with the group data
    if cache_gitlab_group(namespace):

        # we don't process if the project already exists in gitlab
        for project in cacheGroup['projects']:
            if project['name'] == repoName:
                logger.debug('project "{}" already exists in GitLab group "{}"'.format(
                    repoName, namespace))
                return False
        # unsure if the group is intended for mirroring
        # We don't want to mix up human maintained repo with mirrored ones
        if (cacheGroup['meta']['description'] == 'type: mirror'):
            logger.info('would create ' + namespace + '/' + repoName + ' and description ' + repoUrl)

            payload = {
                'namespace_id': cacheGroup['meta']['id'],
                'name': repoName,
                'path': repoName,
                'description': 'origin: {}'.format(repoUrl),
                'shared_runners_enabled': False,
                'lfs_enabled': False,
                'request_access_enabled': False,
                'builds_access_level': 'disabled',
                'wiki_access_level': 'disabled',
                # doesn't work yet in GitLab API
                # 'issues_access_level': 'private' if issuesEnabled else 'disabled',
                'issues_enabled': False,
                'snippets_access_level': 'disabled',
                # doesn't seem implemented yet
                # 'merge_requests_access_level': 'disabled',
                'merge_requests_enabled': False,
                # not implemented in GitLab yet
                # 'analytics_access_level': 'disabled',
                # not implemented in GitLab yet
                # 'operations_access_level': 'disabled',
                'visibility': 'public'
            }

            if config.args.dryrun:
                logger.info('dryrun. The following actions would be made:')
                logger.info('GitLab POST payload {}'.format(payload))
            else:
                logger.info("creating project {}/{} ...".format(namespace, repoName))
                newrepo = requests.post(
                    config.globalCfg['gitlabApiUrl'] + '/projects', params=payload, headers=gitlabHeaders)

                if not newrepo.status_code == 201:  # 201 is "created" in GitLab API
                    logger.debug(pp.pformat(newrepo.json()))
                    logger.error('Error from GitLab (repo "{}/{}"): {}'.format(namespace,
                                                                               repoName, newrepo.json()['message']))

                # tweak issue tracker permissions if needed
                if issuesEnabledFor and (repoName in issuesEnabledFor):
                    # this repo should have issue tracker enabled
                    tweakrepo = requests.put(
                        config.globalCfg['gitlabApiUrl'] + f"/projects/{newrepo.json()['id']}",
                        params={'issues_access_level': 'private'}, headers=gitlabHeaders)
                    if not tweakrepo.status_code == 200:
                        logger.error('Error from GitLab (repo "{}/{}"): {}'.format(namespace,
                                                                                   repoName, tweakrepo.json()['message']))
        else:
            raise GitlabGroupError(
                'group {} description is not configured for mirroring'.format(namespace))
    else:
        logger.error("Can't update group cache for {}".format(namespace))


# print(repos.text)
def is_valid_source(repo, validRepoTypes):
    """
    Determine if the specified repo has special attributes against input list.

    if the input list validRepoTypes is ommited and any of the special attributes
    of ghRepoTypesList is True in the GitHub the repository, then that repo is not considered
    """
    # those are all the possible repo types in GitHub
    ghRepoTypesList = ['archived', 'fork', 'private']

    # from the repo , we extract value for those keys, as a dict
    # ie. { 'archived': False, 'fork': False, 'private': True }
    repoTypes = {k: v for k, v in repo.items() if k in ghRepoTypesList}

    # list of type that are found True in repo
    trueTypes = [k for k, v in repoTypes.items() if v]
    # ie. [ 'private' ]

    if trueTypes:
        # this repo has special attribute type in github (not regular "source")
        if validRepoTypes:
            # config is provided
            if list(set(trueTypes) & set(validRepoTypes)):
                # will sync if there is intersection between the actual repo and config
                return True
            else:
                return False
        else:
            # special repo, no config provided : will not sync
            return False
    else:
        # regular repo : will sync
        return True


def print_github_limits(headers):
    if 'X-RateLimit-Remaining' in headers:
        if int(headers['X-RateLimit-Remaining']) < 5:
            logger.warning('GitHub\'s X-RateLimit-Remaining : {}'.format(headers['X-RateLimit-Remaining']))


def prepare_repos(doGetSize=False, **itemSettings):
    # the dest group is the one read from config if specified otherwise we take
    # github's namespace

    # the destination GitLab group could be specified by user in YML.
    # If not, fallback to the github org name
    dstGroup = itemSettings['glDestGroup'] if itemSettings['glDestGroup'] else itemSettings['ghNs']

    if itemSettings['ghRepo']:
        # get a single repo, providing a full name aka namespace/reponame
        # warning : a single repo can be any of type : fork, archive
        # Endpoint is like https://api.github.com/repos/shumatech/BOSSA
        logger.info("single repo sync")
        # TODO factorize github api calls some day
        _ghRepo = '{}/{}'.format(itemSettings['ghNs'], itemSettings['ghRepo'])
        repo = sessionGihubApi.get(
            "https://api.github.com/repos/{}".format(_ghRepo), timeout=5)
        if repo.status_code == requests.codes.ok:
            print_github_limits(repo.headers)

            repo = repo.json()

            if doGetSize:
                return repo['size']
            else:
                add_project_to_gitlab(dstGroup, repo['name'], repo['clone_url'],
                                      issuesEnabledFor=itemSettings['issuesEnabledFor'])
        elif repo.status_code == requests.codes.not_found:
            logger.warning(
                'GitHub repo {} has not been found (404)\nReason: {}'.format(_ghRepo, repo.text))

            # Is it only the repo that got deleted or the whole github org ?
            _ghOrg = sessionGihubApi.get(
                "https://api.github.com/users/{}".format(itemSettings['ghNs']), timeout=5)
            if _ghOrg.status_code == requests.codes.not_found:
                # the whole org doesn't exist
                raise (GithubError(
                    "The GitHub org {} not found (deleted?)".format(itemSettings['ghNs'])))
            elif _ghOrg.status_code == requests.codes.ok:
                # only the repo has been deleted
                raise (GithubError("GitHub org '{}' exists but repo '{}' has not been found from it (deleted?) (404)\nReason: {}".format(
                    itemSettings['ghNs'], _ghRepo, repo.text)))

                # TODO : Do something to delete the repos from GitLab, if exists
                # TODO : Because if it exists in GitLab but no more in GitHub, it means its been deleted
        else:
            logger.error('unhandled github HTTP return code')

    else:
        # a whole namespace
        repos = sessionGihubApi.get(
            "https://api.github.com/users/{}/repos?per_page=100".format(itemSettings['ghNs']), timeout=5)
        if repos.status_code == requests.codes.ok:
            print_github_limits(repos.headers)

            ghRepos = {}
            reposSize = 0

            # there might be several pages but in any case, we retrieve the item of the first page returned

            for repo in repos.json():
                if is_valid_source(repo, itemSettings['incRepoType']):
                    ghRepos[repo['name']] = repo['clone_url']
                    if not itemSettings['ghRepoOnlyList'] or (itemSettings['ghRepoOnlyList'] and repo['name'] in itemSettings['ghRepoOnlyList']):
                        reposSize += repo['size']

            # next pages if any. Takes advantage of requests's links dict.
            while 'next' in repos.links:
                repos = sessionGihubApi.get(
                    repos.links['next']['url'], timeout=5)

                if repos.status_code != requests.codes.ok:
                    logger.critical(f"can't get next page from GitHub API : {repos.text}")
                    exit()

                for repo in repos.json():
                    if is_valid_source(repo, itemSettings['incRepoType']):
                        ghRepos[repo['name']] = repo['clone_url']
                        if not itemSettings['ghRepoOnlyList'] or (itemSettings['ghRepoOnlyList'] and repo['name'] in itemSettings['ghRepoOnlyList']):
                            reposSize += repo['size']

            logger.info("Considering {} repositories in this github org".format(
                len(ghRepos)))
            if itemSettings['ghRepoOnlyList']:
                logger.info("only {} of them will be sync'ed".format(
                    len(itemSettings['ghRepoOnlyList'])))

            if cache_gitlab_group(dstGroup):
                pInGitLab = [p['name'] for p in cacheGroup['projects']]
                # we sort out if the configured repos are still in GitHub ('only' list)
                if itemSettings['ghRepoOnlyList']:
                    if set(itemSettings['ghRepoOnlyList']) <= set(pInGitLab):
                        logger.info("all configured repos are in Github")

                    _orphans = set(pInGitLab) - set(itemSettings['ghRepoOnlyList'])
                    if _orphans:
                        logger.warning(
                            'Warning, "ONLY" list:  some of the existing GitLab repos are not specified in the source configuration. See orphan log.')
                        _pApiUrls = {p['path_with_namespace']: '{}/projects/{}'.format(config.globalCfg['gitlabApiUrl'], p['id']) for p in cacheGroup['projects']
                                     for orphan in _orphans if orphan == p['name'].lower()}
                        record_orphan(_pApiUrls)

                else:
                    # If the whole org is configured for sync
                    # we print out repos that are in GitLab but not in GitHub (anymore)
                    _reposNotInGH = list(set(pInGitLab) - set(ghRepos.keys()))
                    if _reposNotInGH:
                        _pApiUrls = {p['path_with_namespace']: '{}/projects/{}'.format(config.globalCfg['gitlabApiUrl'], p['id']) for p in cacheGroup['projects']
                                     for orphan in _reposNotInGH if orphan == p['name']}
                        record_orphan(_pApiUrls)

                logger.info(
                    "Some of the GitLab repos may not exist or have special"
                    "status in GitHub org (archive, fork), and will be added to the orphan log.\n")

            for repoName, repoUrl in ghRepos.items():
                if doGetSize:
                    return reposSize
                # filtering against the 'only' list
                # we add the project to gitlab if either:
                # - there is no 'only' list at all (means we add all repos)
                # - if there is a 'only' list AND the github repo match the one we want to sync
                if not itemSettings['ghRepoOnlyList'] or (itemSettings['ghRepoOnlyList'] and repoName in itemSettings['ghRepoOnlyList']):
                    add_project_to_gitlab(dstGroup, repoName, repoUrl,
                                          issuesEnabledFor=itemSettings['issuesEnabledFor'])
        elif repos.status_code == requests.codes.not_found:
            # This is the case where no repos where found in the GitHub org
            # It could be a typo in the source definition, or, that the GitHub Org has been deleted

            if cache_gitlab_group(dstGroup):
                # GitLab group exists
                logger.warning("existing GitLab group '{}' (id: {}) doesn't have a valid matching GitHub org source (currently {})".format(
                    cacheGroup['meta']['name'], cacheGroup['meta']['id'], itemSettings['ghNs']))

            raise (GithubError(
                'No repos found in GitHub org {} (404)\nReason: {}'.format(itemSettings['ghNs'], repos.text)))
        else:
            logger.warning('unhandled github HTTP return code')


def prepare_repos_from_urls(urls, dstGroup, issuesEnabledProjects):
    for url in urls:
        repoName = url.split('/').pop().split('.')[0]
        add_project_to_gitlab(dstGroup, repoName, url, issuesEnabledFor=issuesEnabledProjects)
