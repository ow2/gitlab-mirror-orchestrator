"""Bootstrap the configuration settings."""
from configparser import ConfigParser, ExtendedInterpolation
from sys import exit
import os
import argparse


parser = argparse.ArgumentParser(description="Git to GitLab mirroring tool")

# group = parser.add_mutually_exclusive_group(required=True)
parser.add_argument("-c", "--config-file",
                    help="use specific config file (default: config.cfg)", default="config.cfg")
parser.add_argument("-gs", "--show-groupsize",
                    help="show the total size of the repos in github", action="store_true")
parser.add_argument(
    "-n", "--dryrun", help="Only print what would be made - do not act", action="store_true")
args = parser.parse_args()

base_dir = f'{os.path.dirname(__file__)}/{os.pardir}'
_config_file_path = f'{base_dir}/{args.config_file}'
config_file = os.path.abspath(_config_file_path)

if os.path.isfile(config_file):
    print(f'Configuration file is: {config_file}')
else:
    exit(f'The specified config file {config_file} cannot be found')

cfg = ConfigParser(interpolation=ExtendedInterpolation())
cfg.read(config_file)

# Check config option

required_options = {
    'global': set(['debuglevel', 'gitlabapikey', 'gitlaburl',
                   'gitlabapiversion', 'gitlabapiurl']),
    'git-mirror': set(['gitmirrortoolpath', 'gitmirrordatadir']),
}

wrong_config = False
for section, req_opts in required_options.items():
    missing = req_opts - set(cfg.options(section))
    if missing:
        wrong_config = True
        print(f'these config directives in section [{section}] are missing: {missing}')

if wrong_config:
    exit('please check configuration file.')

# after checking option we can initialize constants
globalCfg = cfg['global']
git_mirror_cfg = cfg['git-mirror']

# CONSTANTS

continue_to_next_item_msg = 'continuing to next item'
