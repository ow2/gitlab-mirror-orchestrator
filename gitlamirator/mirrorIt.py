#!/usr/bin/python3
# coding: utf8

# Copyright (c) 2017-present OW2 http://www.ow2.org
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pprint
import subprocess
import os
from . import config
from .GitHubLab import prepare_repos, prepare_repos_from_urls, GitlabGroupError, GithubError
import yaml
import logging
logger = logging.getLogger(__name__)


pp = pprint.PrettyPrinter()

"""
# big picture of mirroring github to gitlab

# 1. GitLab : An admin create the destination group from GitLab UI and grant 'mirror' user with
# maintainer role.

# 2. Use this script to prepare destination repos (aka projects) in Gitlab. It will adjust the
# project description with the "origin" field and the URL of the remote repo

# 3. use mirror tool (https://github.com/bachp/git-mirror) which takes a GitLab
# group as parameter and mirror all contained projects
"""


def get_issues_enabled_for(item):
    """Get the a list of GitLab projects for which the issues feature should be enabled."""
    if 'issues_enabled_for' in item:
        if isinstance(item['issues_enabled_for'], list):
            return item['issues_enabled_for']
        else:
            return None
    else:
        # issues tracker disabled by default
        return False


def build_ref_spec_args(refspec: str) -> list:
    """
    Take a refspec mapping in the format <refspec>,<refspec>.

    Returns the refspec list arguments as expected by git-mirror
    """
    refspec_args_list = []
    if not refspec:
        return refspec_args_list

    refs = refspec.split(',')
    refspec_arg_name = '--refspec'

    for _ref in refs:
        refspec_args_list.append(refspec_arg_name)
        refspec_args_list.append(_ref)

    return refspec_args_list


def get_src_type(item):
    """
    Returns source type of current item.

    Raise an error if source type is not specified or if several source types are specified for given item.
    """
    # Creating a dictionary with source type as key and a boolean as value (True if source type is defined by the item, false otherwise)
    # The dictionary include all supported source type
    # Examples of a valid dictionary value: {'ghsrc': True, 'directsrc': False}, {'ghsrc': False, 'directsrc': True}
    # Examples of a invalid dictionary value: {'ghsrc': True, 'directsrc': True}, {'ghsrc': False, 'directsrc': False}
    item_source_types = {src_type: True if src_type in item else False for src_type in ['ghsrc', 'directsrc']}

    logger.debug('Item source type: {}'.format(item_source_types))

    # Perform source type validation
    # Get an iterator on a view of sources types dictionary values
    values_iterator = iter(item_source_types.values())
    # Looking for a value set to True and store True in has_one_src_type if we found one
    has_one_src_type = any(values_iterator)
    # Keep looking for another value set to True and store True in has_multiple_src_type if we find one
    has_multiple_src_type = any(values_iterator)

    # If several source type are founded in the item this his an error as they are mutually exclusive
    if has_multiple_src_type:
        raise ValueError('Only one type of source is allowed (ghsrc for GitHub or directsrc for Direct). Found several for: {}'.format(item))
    # If we don't have any source type defined in the configuration
    elif not has_one_src_type:
        raise ValueError('No source type found for: {}'.format(item))
    # We have a single source type defined
    else:
        # Extract the source type from the configuration and return it
        return [s for s, found in item_source_types.items() if found].pop()


def gen_settings(item):
    """
    Take an item as read from repos configuration file.

    Process with various sanitation and parsing
    Return a dict item with key/values that are ready to use for later steps
    """
    def get_github_parts(src_string):
        # Split the GitHub source setting value on '/' character to figure out if we dealing with a GitHub organization/user or a single repository
        src_parts = item['ghsrc'].split('/')
        # If we have two parts in the setting value it means we target a specific GitHub repository within an organization or a user account
        if len(src_parts) == 2:  # ie ghsrc: bachp/git-mirror
            # If the 'only' option was specified it means that we want to only synchronize some repositories.
            # This option does not make sense when synchronizing a single repository so we raise an error.
            if 'only' in item:
                logger.error("conflict: can't use 'only' along with specific repository")
                raise ValueError
            else:
                # We have a valid setting. We store the name of the repository in gh_repo and the name of the organization/user in gh_ns
                gh_ns, gh_repo = src_parts
        # If we don't have a '/' in the ghsrc value, it means that we want to synchronize a whole GitHub organization / all repositories of a user
        elif len(src_parts) == 1:
            # We store the name of the organization / user in gh_ns and None in gh_repo
            gh_ns, gh_repo = src_parts[0], None
        else:
            raise ValueError('malformed ghsrc value')
        return gh_ns, gh_repo

    # Get the list of repositories for which we want to enable the issues tracker feature in GitLab.
    # If we don't enable the feature for any of the project returned value will be False.
    # If we get an error return value is None
    issues_enabled_projects = get_issues_enabled_for(item)
    if issues_enabled_projects is None:
        raise ValueError(
            'repo {} : issues_enabled_for should be a list of gitlab project(s)'.format(item))

    src_type = get_src_type(item)

    logger.info(f'* Processing source entry {item[src_type]}...')

    # adding a specific gitlab destination group if configured so in yaml
    gl_dest_group = item['dstgroup'] if 'dstgroup' in item else None
    # TODO : sanitize gl_dest_group

    # If we are synchronizing from GitHub
    if src_type == 'ghsrc':
        # Get the organization / user name and optionnaly (if a specific repository is targeted) the repository name
        gh_ns, gh_repo = get_github_parts(item['ghsrc'])
    # If we are synchronizing from something else then GitHub
    else:
        gh_ns = gh_repo = None

    # If we synchronizing directly from a set of Git repositories
    if src_type == 'directsrc':
        direct_src = item['directsrc']
    else:
        direct_src = None

    item_settings = {
        'glDestGroup': gl_dest_group,
        'directSrc': direct_src,
        'srcType': src_type,
        'ghNs': gh_ns,
        'ghRepo': gh_repo,
        'ghRepoOnlyList': item['only'] if 'only' in item else None,
        'incRepoType': item['includerepotype'] if 'includerepotype' in item else None,
        'issuesEnabledFor': issues_enabled_projects
    }
    logger.debug(pp.pformat(item_settings))
    return item_settings


def prepare_gitlab_repos(item_settings):
    """Prepare the project in GitLab based on the item settings."""
    try:
        if config.args.show_groupsize:
            size = prepare_repos(doGetSize=True, **item_settings)
            logger.info('cumulated repo size is {}'.format(size))
            return None
        else:
            prepare_repos(**item_settings)
    except GitlabGroupError as e:
        logger.error(f'GitLab Error : {e.message}')
        return None
    except GithubError as e:
        logger.error(f'GitHub Error : {e.message}')
        return None
    else:
        return True


def call_git_mirror_tool(gl_dest_group, gh_ns):
    """Invoke the external git-mirror tool for the specified GitLab group."""
    logger.info("sync'ing repos...")
    logger.debug('Trying to sync: {}'.format(gh_ns))
    refspec_args = build_ref_spec_args(
        config.git_mirror_cfg['gitRefSpec'] if 'gitRefSpec' in config.git_mirror_cfg else '')
    if config.args.dryrun:
        logger.info('dryrun. The following actions would be made:')
        logger.info('run gitmirror subprocess')
    else:
        # get SSH Agent socket if any
        if 'SSH_AUTH_SOCK' in os.environ:
            env_ssh_sock = os.environ['SSH_AUTH_SOCK']
        else:
            env_ssh_sock = ''

        try:
            git_mirror_arguments = ['-v', *refspec_args,
                                    '--group', gl_dest_group if gl_dest_group else gh_ns,
                                    '--mirror-dir', config.git_mirror_cfg['gitMirrorDataDir'],
                                    '--url', config.globalCfg['gitlabUrl']]

            # if no gitlab dstgroup specified for the repo, pass github's namespace instead
            toolresult = subprocess.run([config.git_mirror_cfg['gitMirrorToolPath'],
                                         *git_mirror_arguments],
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        check=True, encoding='utf-8',
                                        env={'PRIVATE_TOKEN': config.globalCfg['gitlabApiKey'],
                                             'SSH_AUTH_SOCK': env_ssh_sock})
        except subprocess.CalledProcessError as e:
            # this exception is raised when the command return code is > 0
            logger.error('error for command {}) : {}'.format(e.args, e.stderr))
        except NameError as e:
            logger.error('something was undefined : {}'.format(e))
        else:
            if toolresult.stderr:
                logger.debug(f'git-mirror arguments: {toolresult.args}')
                logger.critical(
                    "** an error was reported with sync'ing group '{}'' : "
                    .format(gl_dest_group if gl_dest_group else gh_ns))
                logger.critical(toolresult.stderr)


def process_direct_src(item_settings):
    """Take an item with direct repository URL and prepare the repo in GitLab."""
    if config.args.show_groupsize:
        # when showing the size we don't act on GitLab or sync anything.
        return None

    if item_settings['ghRepoOnlyList']:
        logger.error("repo {}: 'only' directive not allowed here".format(
            item_settings['directSrc']))
        return None

    if item_settings['glDestGroup']:
        try:
            prepare_repos_from_urls(item_settings['directSrc'],
                                    item_settings['glDestGroup'],
                                    item_settings['issuesEnabledFor'])
        except GitlabGroupError as e:
            logger.error('GitLab Error : {}'.format(e.message))
            return None
        else:
            return True
    else:
        logger.error(
            f"missing a destination group for this direct source entry {item_settings['directSrc']}")
        return None


def main():
    """Define the main entry point of the tool."""
    logger.info('{} Starting mirroring...')

    # Load the list of repositories / organization to be synchronized (sources)
    with open(f'{config.base_dir}/sourceRepos.yaml', 'r') as yamlfile:

        repolist = yaml.safe_load(yamlfile)

        # Iterate over the configuration file
        for item in repolist:
            logger.debug('Processing item: {}'.format(item))
            try:
                # Parse the setting of an item (a GitHub organization, a specific repository, etc.) to be synchronized
                item_settings = gen_settings(item)
            except ValueError:
                logger.error(config.continue_to_next_item_msg)
                continue

            # step 1 : prepare repos in gitLab
            if ('ghsrc' in item and not prepare_gitlab_repos(item_settings) or 'directsrc' in item and not process_direct_src(item_settings)):
                logger.warning(config.continue_to_next_item_msg)
                continue

            # step 2 : run the mirror tool against prepared Gitlab projects

            call_git_mirror_tool(item_settings['glDestGroup'], item_settings['ghNs'])
            logger.info('mirroring ended')
